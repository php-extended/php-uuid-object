# php-extended/php-uuid-object
An implementation of the php-uuid-interface library

![coverage](https://gitlab.com/php-extended/php-uuid-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-uuid-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-uuid-object ^8`


## Basic Usage

This library may be used the following way :

```php

use PhpExtended\Uuid\Uuid;

$uuid = new Uuid(0x01234567, 0x89ab, 0xcdef, 0xfe, 0xdc, 0xba9876, 0x543210);

$uuid->__toString(); // 01234567-89ab-cdef-fedc-ba9876543210

```


To parse an uuid :

```php

use PhpExtended\Uuid\UuidParser;

$parser = new UuidParser();
$uuid = $parser->parse('<put here your uuid string>');
// $uuid instanceof UuidInterface

```

To create uuid on demand, do :

```php

use PhpExtended\Uuid\UuidV4Factory();

$factory = new UuidV4Factory();
$uuid = $factory->create(); // 5F935265-8863-4672-FAB9-27097EF73660

```


## License

MIT (See [license file](LICENSE)).
