<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uuid-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Uuid;

use PhpExtended\Factory\AbstractFactory;
use Random\RandomException;
use Throwable;

/**
 * UuidV6Factory class file.
 *
 * This factory creates version 6 of uuid objects.
 *
 * @author Anastaszor
 * @extends \PhpExtended\Factory\AbstractFactory<UuidInterface>
 */
class UuidV6Factory extends AbstractFactory implements UuidFactoryInterface
{
	
	/**
	 * Time (in 100ns steps) between the start of the UTC and Unix epochs.
	 *
	 * @var integer
	 */
	public const INTERVAL = 0x01B21DD213814000;
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Factory\FactoryInterface::create()
	 * @throws RandomException
	 */
	public function create() : UuidInterface
	{
		/*
		 * Get time since Gregorian calendar reform in 100ns intervals
		 * This is exceedingly difficult because of PHP's (and pack()'s)
		 * integer size limits.
		 * Note that this will never be more accurate than to the microsecond.
		 */
		$time = ((int) (\microtime(true) * (float) 10000000)) + self::INTERVAL;
		
		// And now to a 64-bit binary representation
		$time = \base_convert((string) $time, 10, 16);
		$hex = (string) \pack('H*', \str_pad($time, 16, '0', \STR_PAD_LEFT));
		
		try
		{
			return new Uuid(
				(\ord($hex[0]) << 24) + (\ord($hex[1]) << 16) + (\ord($hex[2]) << 8) + (\ord($hex[3])),
				(\ord($hex[4]) << 8) + (\ord($hex[5])),
				// four most significant bits holds version number
				((\ord($hex[6]) << 8) + (\ord($hex[7])) & 0x0FFF) | 0x6000,
				// two most significant bits holds zero and one for variant DCE1.1
				\random_int(0, 0x3F) | 0x80,
				\random_int(0, 0xFF),
				\random_int(0, 0xFFFFFF),
				\random_int(0, 0xFFFFFF),
			);
		}
		catch(Throwable $exc)
		{
			// TODO remove php8.3+
			throw new RandomException('Wrapped random exception', -1, $exc);
		}
	}
	
}
