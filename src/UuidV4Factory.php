<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uuid-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Uuid;

use PhpExtended\Factory\AbstractFactory;
use Random\RandomException;
use Throwable;

/**
 * UuidV4Factory class file.
 *
 * This factory creates version 4 of the uuid objects.
 *
 * @author Anastaszor
 * @extends \PhpExtended\Factory\AbstractFactory<UuidInterface>
 */
class UuidV4Factory extends AbstractFactory implements UuidFactoryInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Factory\FactoryInterface::create()
	 * @throws RandomException
	 */
	public function create() : UuidInterface
	{
		try
		{
			return new Uuid(
				\random_int(0, 0xFFFFFFFF),
				\random_int(0, 0xFFFF),
				// four most significant bits holds version number
				\random_int(0, 0x0FFF) | 0x4000,
				// two most significant bits holds zero and one for variant DCE1.1
				\random_int(0, 0x3F) | 0x80,
				\random_int(0, 0xFF),
				\random_int(0, 0xFFFFFF),
				\random_int(0, 0xFFFFFF),
			);
		}
		catch(Throwable $exc)
		{
			// TODO remove php8.3+
			throw new RandomException('Wrapped random exception', -1, $exc);
		}
	}
	
}
