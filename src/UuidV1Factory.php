<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uuid-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Uuid;

use PhpExtended\Factory\AbstractFactory;
use Random\RandomException;
use Throwable;

/**
 * UuidV1Factory class file.
 * 
 * This factory creates version 1 of uuid objects.
 * 
 * @author Anastaszor
 * @extends \PhpExtended\Factory\AbstractFactory<UuidInterface>
 */
class UuidV1Factory extends AbstractFactory implements UuidFactoryInterface
{
	
	/**
	 * Time (in 100ns steps) between the start of the UTC and Unix epochs.
	 * 
	 * @var integer
	 */
	public const INTERVAL = 0x01B21DD213814000;
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Factory\FactoryInterface::create()
	 * @throws RandomException
	 */
	public function create() : UuidInterface
	{
		/*
		 * Get time since Gregorian calendar reform in 100ns intervals
		 * This is exceedingly difficult because of PHP's (and pack()'s)
		 * integer size limits.
		 * Note that this will never be more accurate than to the microsecond.
		 */
		$time = (string) (((int) (\microtime(true) * (float) 10000000)) + (int) self::INTERVAL);
		
		// And now to a 64-bit binary representation
		$time = \base_convert((string) $time, 10, 16);
		$time = (string) \pack('H*', \str_pad($time, 16, '0', \STR_PAD_LEFT));
		
		// Reorder bytes to their proper locations in the UUID
		// [4][5][6][7][2][3][0][1]
		
		try
		{
			return new Uuid(
				(\ord($time[4]) << 24) + (\ord($time[5]) << 16) + (\ord($time[6]) << 8) + (\ord($time[7])),
				(\ord($time[2]) << 8) + (\ord($time[3])),
				// four most significant bits holds version number
				(((\ord($time[0]) << 8) + (\ord($time[1]))) & 0x0FFF) | 0x1000,
				// two most significant bits holds zero and one for variant DCE1.1
				\random_int(0, 0x3F) | 0x80,
				\random_int(0, 0xFF),
				\random_int(0, 0xFFFFFF),
				\random_int(0, 0xFFFFFF),
			);
		}
		catch(Throwable $exc)
		{
			// TODO remove php8.3+
			throw new RandomException('Wrapped random exception', -1, $exc);
		}
	}
	
}
