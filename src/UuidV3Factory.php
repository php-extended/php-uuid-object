<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uuid-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Uuid;

use PhpExtended\Factory\AbstractFactory;

/**
 * UuidV3Factory class file.
 * 
 * This factory creates version 3 of the uuid objects.
 * 
 * @author Anastaszor
 * @extends \PhpExtended\Factory\AbstractFactory<UuidInterface>
 */
class UuidV3Factory extends AbstractFactory implements UuidFactoryInterface
{
	
	/**
	 * The binary string representation of the namespace.
	 * 
	 * @var string
	 */
	protected string $_bytes;
	
	/**
	 * The current name of the.
	 * 
	 * @var string
	 */
	protected string $_name = '';
	
	/**
	 * builds a new UuidV3Factory with the given namespace.
	 * 
	 * @param UuidInterface $namespace
	 */
	public function __construct(UuidInterface $namespace)
	{
		$this->_bytes = $namespace->toBinaryString();
	}
	
	/**
	 * Sets the name to be converted to uuid.
	 * 
	 * @param string $name
	 */
	public function setName(string $name) : void
	{
		$this->_name = $name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Factory\FactoryInterface::create()
	 */
	public function create() : UuidInterface
	{
		$hex = \md5($this->_bytes.$this->_name, true);
		
		return new Uuid(
			(\ord($hex[0]) << 24) + (\ord($hex[1]) << 16) + (\ord($hex[2]) << 8) + (\ord($hex[3])),
			(\ord($hex[4]) << 8) + (\ord($hex[5])),
			// four most significant bits holds version number
			((\ord($hex[6]) << 8) + (\ord($hex[7])) & 0x0FFF) | 0x3000,
			// two most significant bits holds zero and one for variant DCE1.1
			(\ord($hex[8]) & 0x3F) | 0x80,
			(\ord($hex[9])),
			(\ord($hex[10]) << 16) + (\ord($hex[11]) << 8) + (\ord($hex[12])),
			(\ord($hex[13]) << 16) + (\ord($hex[14]) << 8) + (\ord($hex[15])),
		);
	}
	
}
