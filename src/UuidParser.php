<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uuid-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Uuid;

use PhpExtended\Lexer\LexerInterface;
use PhpExtended\Parser\AbstractParserLexer;

/**
 * UuidParser class file.
 * 
 * This class is a parser for uuids.
 * 
 * @author Anastaszor
 * @extends AbstractParserLexer<UuidInterface>
 */
class UuidParser extends AbstractParserLexer implements UuidParserInterface
{
	
	public const L_HEXA = 1;
	public const L_DUAL = 2;
	public const L_DASH = 3;
	
	/**
	 * Builds a new UuidParser with the given lexer config.
	 */
	public function __construct()
	{
		parent::__construct(UuidInterface::class);
		$this->_config->addMappings(LexerInterface::CLASS_HEXA, self::L_HEXA);
		$this->_config->addMappings('-', self::L_DASH);
		$this->_config->addMerging(self::L_HEXA, self::L_HEXA, self::L_DUAL);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : UuidInterface
	{
		return parent::parse($data); // interface compatibility
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\AbstractParserLexer::parseLexer()
	 */
	public function parseLexer(LexerInterface $lexer) : UuidInterface
	{
		$lexer->rewind();
		
		$hexs = [];
		$token = null;
		
		for($i = 0; 16 > $i; $i++)
		{
			if(4 === $i || 6 === $i || 8 === $i || 10 === $i)
			{
				$token = $this->expectOneOf($lexer, [self::L_DASH], $token);
			}
			$token = $this->expectOneOf($lexer, [self::L_DUAL], $token);
			$hexs[$i] = (int) \hexdec($token->getData());
		}
		
		$this->expectEof($lexer, $token);
		
		/** @psalm-suppress PossiblyUndefinedIntArrayOffset */
		$timeLow = ($hexs[0] << 24) + ($hexs[1] << 16) + ($hexs[2] << 8) + ($hexs[3]);
		/** @psalm-suppress PossiblyUndefinedIntArrayOffset */
		$timeMid = ($hexs[4] << 8) + ($hexs[5]);
		/** @psalm-suppress PossiblyUndefinedIntArrayOffset */
		$timeHighVersion = ($hexs[6] << 8) + ($hexs[7]);
		/** @psalm-suppress PossiblyUndefinedIntArrayOffset */
		$clockSeqHiReserved = ($hexs[8]);
		/** @psalm-suppress PossiblyUndefinedIntArrayOffset */
		$clockSeqLow = ($hexs[9]);
		/** @psalm-suppress PossiblyUndefinedIntArrayOffset */
		$nodeHigh = ($hexs[10] << 16) + ($hexs[11] << 8) + ($hexs[12]);
		/** @psalm-suppress PossiblyUndefinedIntArrayOffset */
		$nodeLow = ($hexs[13] << 16) + ($hexs[14] << 8) + ($hexs[15]);
		
		return new Uuid($timeLow, $timeMid, $timeHighVersion, $clockSeqHiReserved, $clockSeqLow, $nodeHigh, $nodeLow);
	}
	
}
