<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uuid-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Uuid;

/**
 * Uuid class file.
 * 
 * This class is made to manipulate uuids.
 * 
 * @author Anastaszor
 */
class Uuid implements UuidInterface
{
	
	/**
	 * The low field of the timestamp, represented as 32 bit integer.
	 *
	 * @var integer [0, 2^32-1]
	 */
	protected int $_timeLow = 0;
	
	/**
	 * The middle field of the timestamp, represented as 16 bit integer.
	 *
	 * @var integer [0, 2^16-1]
	 */
	protected int $_timeMid = 0;
	
	/**
	 * The high field of the timestamp multiplexed with the version number,
	 * represented as 16 bit integer.
	 *
	 * @var integer [0, 2^16-1]
	 */
	protected int $_timeHighVersion = 0;
	
	/**
	 * The high field of the clock sequence multiplexed with the variant,
	 * represented as 8 bit integer.
	 *
	 * @var integer [0, 255]
	 */
	protected int $_clockSeqHiReserved = 0;
	
	/**
	 * The low field of the clock sequence, represented as 8 bit integer.
	 *
	 * @var integer [0, 255]
	 */
	protected int $_clockSeqLow = 0;
	
	/**
	 * The spacially unique node identifier, first part, represented as 24 bit
	 * integer.
	 *
	 * @var integer [0, 2^24-1]
	 */
	protected int $_nodeHigh = 0;
	
	/**
	 * The spacially unique node identifier, second part, represented as 24 bit
	 * identifier.
	 *
	 * @var integer [0, 2^24-1]
	 */
	protected int $_nodeLow = 0;
	
	/**
	 * Builds a new Uuid from its integer components.
	 * 
	 * @param integer $timeLow [0, 2^32-1] 4bytes
	 * @param integer $timeMid [0, 2^16-1] 2bytes
	 * @param integer $timeHighVersion [0, 2^16-1] 2bytes
	 * @param integer $clockSeqHiReserved [0, 255] 1byte
	 * @param integer $clockSeqLow [0, 255] 1byte
	 * @param integer $nodeHigh [0, 2^24-1] 3bytes
	 * @param integer $nodeLow [0, 2^24-1] 3bytes
	 */
	public function __construct(int $timeLow, int $timeMid, int $timeHighVersion, int $clockSeqHiReserved, int $clockSeqLow, int $nodeHigh, int $nodeLow)
	{
		$this->_timeLow = $timeLow & 0xFFFFFFFF;
		$this->_timeMid = $timeMid & 0xFFFF;
		$this->_timeHighVersion = $timeHighVersion & 0xFFFF;
		$this->_clockSeqHiReserved = $clockSeqHiReserved & 0xFF;
		$this->_clockSeqLow = $clockSeqLow & 0xFF;
		$this->_nodeHigh = $nodeHigh & 0xFFFFFF;
		$this->_nodeLow = $nodeLow & 0xFFFFFF;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->getCanonicalRepresentation();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Uuid\UuidInterface::getCanonicalRepresentation()
	 */
	public function getCanonicalRepresentation() : string
	{
		return \str_pad(\dechex($this->_timeLow), 8, '0', \STR_PAD_LEFT)
			.'-'.\str_pad(\dechex($this->_timeMid), 4, '0', \STR_PAD_LEFT)
			.'-'.\str_pad(\dechex($this->_timeHighVersion), 4, '0', \STR_PAD_LEFT)
			.'-'.\str_pad(\dechex($this->_clockSeqHiReserved), 2, '0', \STR_PAD_LEFT)
				.\str_pad(\dechex($this->_clockSeqLow), 2, '0', \STR_PAD_LEFT)
			.'-'.\str_pad(\dechex($this->_nodeHigh), 6, '0', \STR_PAD_LEFT)
				.\str_pad(\dechex($this->_nodeLow), 6, '0', \STR_PAD_LEFT);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Uuid\UuidInterface::getTimeLow()
	 */
	public function getTimeLow() : int
	{
		return $this->_timeLow;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Uuid\UuidInterface::getTimeMid()
	 */
	public function getTimeMid() : int
	{
		return $this->_timeMid;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Uuid\UuidInterface::getTimeHigh()
	 */
	public function getTimeHigh() : int
	{
		return $this->_timeHighVersion & 0x0FFF;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Uuid\UuidInterface::getClockSeqHigh()
	 */
	public function getClockSeqHigh() : int
	{
		return $this->_clockSeqHiReserved;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Uuid\UuidInterface::getVersion()
	 */
	public function getVersion() : int
	{
		return ($this->_timeHighVersion & 0xF000) >> 12;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Uuid\UuidInterface::getClockSeqLow()
	 */
	public function getClockSeqLow() : int
	{
		return $this->_clockSeqLow;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Uuid\UuidInterface::getNodeHigh()
	 */
	public function getNodeHigh() : int
	{
		return $this->_nodeHigh;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Uuid\UuidInterface::getNodeLow()
	 */
	public function getNodeLow() : int
	{
		return $this->_nodeLow;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Uuid\UuidInterface::__toBinaryString()
	 */
	public function toBinaryString() : string
	{
		$hex = $this->toHexString();
		$bin = '';
		
		for($i = 0; \mb_strlen($hex) > $i; $i += 2)
		{
			$bin .= \chr((int) \hexdec($hex[$i].$hex[$i + 1]));
		}
		
		return $bin;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Uuid\UuidInterface::__toHexString()
	 */
	public function toHexString() : string
	{
		return \str_pad(\dechex($this->_timeLow), 8, '0', \STR_PAD_LEFT)
			.\str_pad(\dechex($this->_timeMid), 4, '0', \STR_PAD_LEFT)
			.\str_pad(\dechex($this->_timeHighVersion), 4, '0', \STR_PAD_LEFT)
			.\str_pad(\dechex($this->_clockSeqHiReserved), 2, '0', \STR_PAD_LEFT)
			.\str_pad(\dechex($this->_clockSeqLow), 2, '0', \STR_PAD_LEFT)
			.\str_pad(\dechex($this->_nodeHigh), 6, '0', \STR_PAD_LEFT)
			.\str_pad(\dechex($this->_nodeLow), 6, '0', \STR_PAD_LEFT);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Uuid\UuidInterface::__toArray()
	 */
	public function toArray() : array
	{
		return [
			$this->_timeLow,
			$this->_timeMid,
			$this->_timeHighVersion,
			$this->_clockSeqHiReserved,
			$this->_clockSeqLow,
			$this->_nodeHigh,
			$this->_nodeLow,
		];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Uuid\UuidInterface::equals()
	 */
	public function equals($uuid) : bool
	{
		return $uuid instanceof UuidInterface
			&& $this->getTimeLow() === $uuid->getTimeLow()
			&& $this->getTimeMid() === $uuid->getTimeMid()
			&& $this->getTimeHigh() === $uuid->getTimeHigh()
			&& $this->getClockSeqHigh() === $uuid->getClockSeqHigh()
			&& $this->getVersion() === $uuid->getVersion()
			&& $this->getClockSeqLow() === $uuid->getClockSeqLow()
			&& $this->getNodeHigh() === $uuid->getNodeHigh()
			&& $this->getNodeLow() === $uuid->getNodeLow();
	}
	
}
