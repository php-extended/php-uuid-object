<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uuid-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Uuid\UuidV1Factory;
use PHPUnit\Framework\TestCase;

/**
 * UuidV1FactoryTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Uuid\UuidV1Factory
 *
 * @internal
 *
 * @small
 */
class UuidV1FactoryTest extends TestCase
{
	
	/**
	 * The factory to test.
	 * 
	 * @var UuidV1Factory
	 */
	protected UuidV1Factory $_factory;
	
	public function testItWorks() : void
	{
		$uuid = $this->_factory->create();
		
		$this->assertEquals(1, $uuid->getVersion());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_factory = new UuidV1Factory();
	}
	
}
