<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uuid-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Uuid\UuidV6Factory;
use PHPUnit\Framework\TestCase;

/**
 * UuidV6FactoryTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Uuid\UuidV6Factory
 *
 * @internal
 *
 * @small
 */
class UuidV6FactoryTest extends TestCase
{
	
	/**
	 * The factory to test.
	 * 
	 * @var UuidV6Factory
	 */
	protected UuidV6Factory $_factory;
	
	public function testItWorks() : void
	{
		$uuid = $this->_factory->create();
		
		$this->assertEquals(6, $uuid->getVersion());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_factory = new UuidV6Factory();
	}
	
}
