<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uuid-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Parser\ParseException;
use PhpExtended\Uuid\UuidInterface;
use PhpExtended\Uuid\UuidParser;
use PHPUnit\Framework\TestCase;

/**
 * UuidParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Uuid\UuidParser
 *
 * @internal
 *
 * @small
 */
class UuidParserTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UuidParser
	 */
	protected UuidParser $_parser;
	
	public function testNilUuid() : void
	{
		$this->assertEquals(UuidInterface::NS_NULL, $this->_parser->parse(UuidInterface::NS_NULL)->__toString());
	}
	
	public function testNsDns() : void
	{
		$this->assertEquals(UuidInterface::NS_DNS, $this->_parser->parse(UuidInterface::NS_DNS)->__toString());
	}
	
	public function testNsUrl() : void
	{
		$this->assertEquals(UuidInterface::NS_URL, $this->_parser->parse(UuidInterface::NS_URL)->__toString());
	}
	
	public function testNsOid() : void
	{
		$this->assertEquals(UuidInterface::NS_OID, $this->_parser->parse(UuidInterface::NS_OID)->__toString());
	}
	
	public function testNsX500() : void
	{
		$this->assertEquals(UuidInterface::NS_X500, $this->_parser->parse(UuidInterface::NS_X500)->__toString());
	}
	
	public function testThrow() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse(null);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_parser = new UuidParser();
	}
	
}
