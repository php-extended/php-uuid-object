<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uuid-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Uuid\Uuid;
use PhpExtended\Uuid\UuidV3Factory;
use PHPUnit\Framework\TestCase;

/**
 * UuidV3FactoryTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Uuid\UuidV3Factory
 *
 * @internal
 *
 * @small
 */
class UuidV3FactoryTest extends TestCase
{	
	/**
	 * The factory to test.
	 * 
	 * @var UuidV3Factory
	 */
	protected UuidV3Factory $_factory;
	
	public function testItWorks() : void
	{
		$this->_factory->setName('a new name');
		$uuid = $this->_factory->create();
		
		$this->assertEquals(3, $uuid->getVersion());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_factory = new UuidV3Factory(new Uuid(0, 0, 0, 0, 0, 0, 0));
	}
	
}
