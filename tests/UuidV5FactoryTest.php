<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uuid-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Uuid\Uuid;
use PhpExtended\Uuid\UuidV5Factory;
use PHPUnit\Framework\TestCase;

/**
 * UuidV5FactoryTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Uuid\UuidV5Factory
 *
 * @internal
 *
 * @small
 */
class UuidV5FactoryTest extends TestCase
{
	
	/**
	 * The factory to test.
	 * 
	 * @var UuidV5Factory
	 */
	protected UuidV5Factory $_factory;
	
	public function testItWorks() : void
	{
		$this->_factory->setName('a new name');
		$uuid = $this->_factory->create();
		
		$this->assertEquals(5, $uuid->getVersion());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_factory = new UuidV5Factory(new Uuid(0, 0, 0, 0, 0, 0, 0));
	}
	
}
