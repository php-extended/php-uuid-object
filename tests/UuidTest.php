<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uuid-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Uuid\Uuid;
use PhpExtended\Uuid\UuidInterface;
use PHPUnit\Framework\TestCase;

/**
 * UuidTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Uuid\Uuid
 *
 * @internal
 *
 * @small
 */
class UuidTest extends TestCase
{
	
	/**
	 * The uuid to test.
	 * 
	 * @var UuidInterface
	 */
	protected UuidInterface $_uuid;
	
	public function testToString() : void
	{
		$this->assertEquals('01234567-89ab-cdef-fedc-ba9876543210', $this->_uuid->__toString());
	}
	
	public function testTimeLow() : void
	{
		$this->assertEquals(0x01234567, $this->_uuid->getTimeLow());
	}
	
	public function testTimeMid() : void
	{
		$this->assertEquals(0x89AB, $this->_uuid->getTimeMid());
	}
	
	public function testTimeHigh() : void
	{
		$this->assertEquals(0xDEF, $this->_uuid->getTimeHigh());
	}
	
	public function testClockSeqHigh() : void
	{
		$this->assertEquals(0xFE, $this->_uuid->getClockSeqHigh());
	}
	
	public function testVersion() : void
	{
		$this->assertEquals(0xC, $this->_uuid->getVersion());
	}
	
	public function testClockSeqLow() : void
	{
		$this->assertEquals(0xDC, $this->_uuid->getClockSeqLow());
	}
	
	public function testNodeHigh() : void
	{
		$this->assertEquals(0xBA9876, $this->_uuid->getNodeHigh());
	}
	
	public function testNodeLow() : void
	{
		$this->assertEquals(0x543210, $this->_uuid->getNodeLow());
	}
	
	public function testToBinaryString() : void
	{
		$this->assertEquals("\x01\x23\x45\x67\x89\xab\xcd\xef\xfe\xdc\xba\x98\x76\x54\x32\x10", $this->_uuid->toBinaryString());
	}
	
	public function testToHexString() : void
	{
		$this->assertEquals('0123456789abcdeffedcba9876543210', $this->_uuid->toHexString());
	}
	
	public function testToArray() : void
	{
		$this->assertEquals([
			0x01234567,
			0x89AB,
			0xCDEF,
			0xFE,
			0xDC,
			0xBA9876,
			0x543210,
		], $this->_uuid->toArray());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue(
			$this->_uuid->equals(
				new Uuid(0x01234567, 0x89AB, 0xCDEF, 0xFE, 0xDC, 0xBA9876, 0x543210),
			),
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_uuid = new Uuid(0x01234567, 0x89AB, 0xCDEF, 0xFE, 0xDC, 0xBA9876, 0x543210);
	}
	
}
